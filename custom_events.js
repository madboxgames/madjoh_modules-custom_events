define([
	'require',
	'madjoh_modules/uuid/uuid'
],
function(require, UUID){
	var CustomEvents = {
		addCustomEventListener : function(object, eventName, fonction){
			if(!object || !eventName || !fonction) return console.log('Missing argument');

			if(eventName === 'PageLoaded'){
				if(document.loaded) fonction();
				else document.loadListeners.push(fonction);
			}else if(eventName === 'PageUnloaded'){
				document.unloadListeners.push(fonction);
			}else{
				var id = UUID.get();

				if(!object.customEvents) 			object.customEvents 			= {};
				if(!object.customEvents[eventName]) object.customEvents[eventName] 	= {};
				object.customEvents[eventName][id] = fonction;
				return id;
			}
		},
		removeCustomEventListener : function(object, eventName, id){
			if(!object || !eventName || !id) 							return console.log('Missing argument');
			if(!object.customEvents || !object.customEvents[eventName]) return console.log('Trying to remove a non existent custom listener');
			
			delete object.customEvents[eventName][id];
		},
		fireCustomEvent : function(object, eventName, parameters){
			if(!object || !eventName) console.log('Missing argument');
			
			if(object.customEvents && object.customEvents[eventName]){
				for(var key in object.customEvents[eventName]){
					object.customEvents[eventName][key](object, parameters);
				}
			}
		}
	};
	
    return CustomEvents;
});