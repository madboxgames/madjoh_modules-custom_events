# custom_events #

v1.0.4

** This Documentation is not up to date !**

This module enables the creation of custom events and their attachment to DOM elements.

It contains 3 methods :

- ** addCustomEventListener ** : Attaches the listener.
- ** removeCustomEventListener ** : Removes the listener.
- ** fireCustomEvent ** : Fires the event.

## addCustomEventListener ##
```js
addCustomEventListener(object, eventName, fonction);
```

- ** object ** is DOM element you want to attach the event listener to.
- ** eventName ** is the name of the event the listener needs to watch.
- ** fonction ** is the function to execute after the event is fired.

## removeCustomEventListener ##
```js
removeCustomEventListener(object, eventName, fonction);
```

- ** object ** is DOM element you want to remove the event listener from.
- ** eventName ** is the name of the event the listener was watching.
- ** fonction ** is the function to remove.

## fireCustomEvent ##
```js
fireCustomEvent(object, eventName);
```

- ** object ** is DOM element concerned by the event.
- ** eventName ** is the name of the event to fire.
